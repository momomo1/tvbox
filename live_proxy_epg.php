<?php
header("Content-Type:application/json;chartset=uft-8");
define('SELF', pathinfo(__file__, PATHINFO_BASENAME));
define('FCPATH', str_replace("\\", "/", str_replace(SELF, '', __file__)));
ini_set('display_errors',1);            
ini_set('display_startup_errors',1);   
error_reporting(E_ERROR);
include "caches.class.php";
$t = time();
$start_a = "00:01";
$start_b = "00:10";
$a_t = strtotime($start_a);
$b_t = strtotime($start_b);
if( $t<$a_t || $b_t<$t ){
}else{
	$files = glob(FCPATH  . 'bak/*');
	foreach($files as $file){
        if (is_file($file)) {
            @unlink($file);
        }
    }
}
$cachedir="./cache";
if (! is_dir ( $cachedir )) {
    @mkdir ( $cachedir, 0755, true ) or die ( '创建文件夹失败' );
}
$id=!empty($_GET["id"])?$_GET["id"]:exit(json_encode(["code"=>500,"msg"=>"EPG频道参数不能为空!","name"=>$name,"date"=>null,"data"=>null],JSON_UNESCAPED_UNICODE));

echo out_epg($id);exit;
//输出EPG节目地址
function out_epg($id){
   $tvdata = channel($id);
   $tvid = $tvdata['tvid'];
   $epgid =  $tvdata['epgid'];	
	if (!is_numeric($tvid)) {
	    return $epgid;
	}	
	$tt=cache("time_out_chk","cache_time_out");  //获取当前时间（后天）的00:00时间戳
	if (time()>=$tt) {
	     Cache::$cache_path="./cache/";   //设置缓存路径
		 //删除除当前目录缓存文件
		 Cache::dels();
		 //重新写入当天时间缓存文件
		 cache("time_out_chk","cache_time_out");
	}
	$ejson=cache($epgid,"get_epg_data",[$epgid,$tvid,$id]);
	return $ejson;
} 

//缓存EPG节目数据
function cache($key,$f_name,$ff=[]){
    Cache::$cache_path="./cache/";   //设置缓存路径
	$val=Cache::gets($key);
	if (!$val) {
	    $data=call_user_func_array($f_name,$ff);   
		Cache::put($key,$data);
		return $data;
	}else {
	    return $val;
	}
} 

function cache_time_out(){
    date_default_timezone_set("Asia/Shanghai");
	$tt=strtotime(date("Y-m-d 00:00:00",time()))+86400;
	return $tt;
} 

//频道映射对应表
function channel($id){
	$id=urldecode($id);
	$arr_file="epg_channel_arr.php";
	//映射文件是否存在
	if (!file_exists($arr_file)) {
		$data=["code"=>500,"msg"=>"文件获取失败!","name"=>null,"date"=>null,"data"=>null];
	    exit(json_encode($data,JSON_UNESCAPED_UNICODE));
	}
	//加载映射数组文件
	include ($arr_file);
	if (empty($arr)) {
	    $data=["code"=>500,"msg"=>"频道列表获取失败!","name"=>null,"date"=>null,"data"=>null];
	    exit(json_encode($data,JSON_UNESCAPED_UNICODE));
	}elseif (empty($arr[$id]["epgid"])) {
	    $data=["code"=>500,"msg"=>"频道不存在!","name"=>null,"date"=>null,"data"=>null];
	    exit(json_encode($data,JSON_UNESCAPED_UNICODE));
	}
    return $arr[$id];
}   

$epgid= channel($id)["epgid"];
$tvid= channel($id)["tvid"];
$type= channel($id)["type"];
//获取EPG列表
function get_epg_data($epgid,$tvid){
	 $url="https://api.cntv.cn/epg/epginfo?serviceId=&d=".date('Ymd')."&c=".$epgid;
     $ch=curl_init();
     curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
     $ip = $_SERVER["REMOTE_ADDR"];
     curl_setopt($ch, CURLOPT_URL, $url);
     curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.$ip, 'CLIENT-IP:'.$ip));
	 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
     $str=curl_exec($ch);																																															
	 curl_close($ch);
	 $re=json_decode($str,true);
		if (!empty($re[$epgid]['program'])){
			$data=array("code"=>200,"msg"=>"请求成功!","name"=>$re[$epgid]['channelName'],"tvid"=>$tvid,"date"=>date('Y-m-d'));
			//$data=array("code"=>200,"msg"=>"请求成功!","post"=>20);
			foreach($re[$epgid]['program'] as $row){
				$data["data"][]= array("name"=> $row['t'],"starttime"=> $row['showTime']);
			}
			return  json_encode($data,JSON_UNESCAPED_UNICODE);
		}else{
			
		$data=["code"=>500,"msg"=>"请求失败!","epgid"=>$epgid,"tvid"=>$tvid,"name"=>$name,"date"=>date('Ymd'),"data"=>null];
	   return  json_encode($data,JSON_UNESCAPED_UNICODE);
	}
}
?>
